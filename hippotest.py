from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException
import time, hashlib, unittest, logging, re, HtmlTestRunner

'''__author__ ="John Hollingworth - SWEDWISE"
__version__=""
__maintainer="John hollingworth"
__email__="john.hollingworth@swedwise.se"
__status__="Development"'''

class TestHippo(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        print('Starting all tests')

    @classmethod
    def tearDownClass(self):
        print('Finishing all tests')

    def setUp (self):
        print('starting')
        '''self.driver = webdriver.Chrome('C:/dev/MicrosoftWebDriver.exe')'''
        self.driver = webdriver.Chrome('C:/dev/chromedriver.exe')
        '''self.driver.implicitly_wait(10)'''
        self.prodURL = 'https://integrationer.tjansteplattform.se/'
        self.QAURL ='https://qa.integrationer.tjansteplattform.se/'
        self.md5maj2017 = '5a24edb267327c6f09908946579ca0d4'
        self.currentmd5 = '7e0f68784fba59a64705928de0694478'
        self.qamd5 = '7af32bc17cc75b98756372f3b1c95208'

    def tearDown(self):
        print ('finishing')
        self.driver.close()

        '''    def test_checkCurrentProdMD5(self):
        print('Går till vald URL och hämtar det som finns i tabellen och beräknar en checksum av det. returnerar checksum ')
        self.driver.get(self.prodURL)
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))                
        self.assertEqual(hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest(), self.currentmd5)

    def test_checkCurrentQAMD5(self):
        print('Går till vald URL och hämtar det som finns i tabellen och beräknar en checksum av det. returnerar checksum')
        self.driver.get(self.QAURL)
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
        self.assertEqual(hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest(), self.qamd5) '''
    def test_compareQAwithProd(self):
        print('Hämtar checksum för både QA och prod och jämför dessa. Kommer sannolikt faila.')
        self.driver.get(self.prodURL)
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
        Ptxt = self.driver.find_elements_by_tag_name('tbody')[1].text
        PMD5 = hashlib.md5(Ptxt.encode('utf-8')).hexdigest()
        self.driver.get(self.QAURL)
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
        Qtxt = self.driver.find_elements_by_tag_name('tbody')[1].text
        QMD5 = hashlib.md5(Qtxt.encode('utf-8')).hexdigest()
        self.assertTrue(PMD5 == QMD5)

    def test_checkMay52017MD5(self):
        print('hämtar historisk data (5e maj 2017) som kontrolleras mot känd checksum ')
        self.driver.get(self.prodURL)
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
        self.driver.find_element_by_xpath("//select[@id='dateSelect']/option[@value='2017-05-05']").click()
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
        self.assertEqual(hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest(), self.md5maj2017)

    def test_filterConsumerAndReset(self):
        print('väljer ut en konsument, kontrollerar att filtrering görs, samt återställer och kontrollerar detta')
        self.driver.get(self.prodURL)
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
        fulllength = len(self.driver.find_elements_by_tag_name('tbody')[1].text)
        fulltextChecksum = ''
        fulltextChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
        with self.subTest('kontrollerar filtrering på Tjänstekonsument via klick'):
            self.driver.find_element_by_id('cconsumer:139').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'HSASERVICES-106J'))
            self.partialChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            partialconsumerlength = len(self.driver.find_elements_by_tag_name('tbody')[1].text)
            self.assertTrue(fulllength > partialconsumerlength)
        with self.subTest('kontrollerar återställning'):
            self.driver.find_element_by_id('consumerReset').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            resetChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(fulltextChecksum == resetChecksum)
        with self.subTest('kontrollerar filtrering på Tjänstekonsument via textfält'):
            element = self.driver.find_element_by_id('consumerSelect')
            element.send_keys('HSASERVICES-106J')
            self.driver.find_element_by_id('awesomplete_list_8').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'HSASERVICES'))
            self.partialChecksum2 = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(self.partialChecksum == self.partialChecksum2)
        with self.subTest('kontrollerar återställning'):
            self.driver.find_element_by_id('consumerReset').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            resetChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(fulltextChecksum == resetChecksum)

    def test_filterDomainAndContractAndReset(self):
        print('väljer ut en tjänstedomän, sedan kontrakt, kontrollerar att filtrering görs, samt återställer och kontrollerar detta')
        self.driver.get(self.prodURL)
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
        fulllength = len(self.driver.find_elements_by_tag_name('tbody')[1].text)
        fulltextChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
        with self.subTest('kontrollerar filtrering på Tjänstedomän'):
            self.driver.find_element_by_id('d17').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            partialdomainlength = len(self.driver.find_elements_by_tag_name('tbody')[1].text)
            partialdomainChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(fulllength > partialdomainlength)
        with self.subTest('kontrollerar filtrering på Tjänstekontrakt'):
            self.driver.find_element_by_id('C214').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            partialcontractlength = len(self.driver.find_elements_by_tag_name('tbody')[1].text)
            self.assertTrue(partialdomainlength > partialcontractlength)
        with self.subTest('kontrollerar återställning av tjänstekontrakt'):
            self.driver.find_element_by_id('contractReset').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            resetChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(partialdomainChecksum == resetChecksum)
        with self.subTest('kontrollerar återställning av domän'):
            self.driver.find_element_by_id('domainReset').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            resetChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(fulltextChecksum == resetChecksum)
        with self.subTest('kontrollerar filtrering på Tjänstedomän via textfält'):
            element = self.driver.find_element_by_id('contractSelect')
            element.send_keys('clinicalprocess:activity:request')     
            self.driver.find_element_by_id('awesomplete_list_18').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            partialdomainlength = len(self.driver.find_elements_by_tag_name('tbody')[1].text)
            self.assertTrue(fulllength > partialdomainlength)
        with self.subTest('kontrollerar återställning'):
            self.driver.find_element_by_id('domainReset').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            resetChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(fulltextChecksum == resetChecksum)
        with self.subTest('kontrollerar filtrering på Tjänstekkontrakt via textfält'):
            element = self.driver.find_element_by_id('contractSelect')
            element.send_keys('ProcessRequestOutcome V1')     
            self.driver.find_element_by_id('awesomplete_list_26').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            partialdomainlength = len(self.driver.find_elements_by_tag_name('tbody')[1].text)
            self.assertTrue(fulllength > partialdomainlength)
        with self.subTest('kontrollerar återställning'):
            self.driver.find_element_by_id('contractReset').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            resetChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(fulltextChecksum == resetChecksum)
    def test_filterPlatformAndReset(self):
        print('väljer ut en plattform, kontrollerar att filtrering görs, samt återställer och kontrollerar detta')
        self.driver.get(self.prodURL)
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
        fulllength = len(self.driver.find_elements_by_tag_name('tbody')[1].text)
        fulltextChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
        with self.subTest('kontrollerar filtrering på plattform'):
            self.driver.find_element_by_id('plattformChains2').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            partiallength = len(self.driver.find_elements_by_tag_name('tbody')[1].text)
            self.assertTrue(fulllength > partiallength)
        with self.subTest('kontrollerar återställning'):
            self.driver.find_element_by_id('plattformChainsReset').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            resetChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(fulltextChecksum == resetChecksum)
    def test_filterLAAndReset(self):
        print('väljer ut en logisk adressat, kontrollerar att filtrering görs, samt återställer och kontrollerar detta')
        self.driver.get(self.prodURL)
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
        fulllength = len(self.driver.find_elements_by_tag_name('tbody')[1].text)
        fulltextChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
        with self.subTest('kontrollerar filtrering på logisk adress'):
            self.driver.find_element_by_id('llogicaladdress: 1383').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            self.partialChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            partiallength = len(self.driver.find_elements_by_tag_name('tbody')[1].text)
            self.assertTrue(fulllength > partiallength)
        with self.subTest('kontrollerar återställning'):
            self.driver.find_element_by_id('logicalAddressReset').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            resetChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(fulltextChecksum == resetChecksum)
        with self.subTest('kontrollerar filtrering på logisk addressat via textfält'):
            element = self.driver.find_element_by_id('laSelect')
            element.send_keys('SE2321000016-92V4')     
            self.driver.find_element_by_id('awesomplete_list_10').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            self.partialChecksum2 = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(self.partialChecksum == self.partialChecksum2)
        with self.subTest('kontrollerar återställning'):
            self.driver.find_element_by_id('logicalAddressReset').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
            resetChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(fulltextChecksum == resetChecksum)
    def test_filterProducerAndReset(self):
        print('väljer ut en producent, kontrollerar att filtrering görs, samt återställer och kontrollerar detta')
        self.driver.get(self.prodURL)
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE5565189692'))
        fulllength = len(self.driver.find_elements_by_tag_name('tbody')[1].text)
        fulltextChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
        with self.subTest('kontrollerar filtrering på producent'):
            self.driver.find_element_by_id('pproducer:297').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE5565189692'))
            self.partialChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            partiallength = len(self.driver.find_elements_by_tag_name('tbody')[1].text)
            self.assertTrue(fulllength > partiallength)
        with self.subTest('kontrollerar återställning'):
            self.driver.find_element_by_id('producerReset').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE5565189692'))
            resetChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(fulltextChecksum == resetChecksum)
        with self.subTest('kontrollerar filtrering på producent via textfält'):
            element = self.driver.find_element_by_id('producerSelect')
            element.send_keys('SE2321000016-4HR3')
            self.driver.find_element_by_id('awesomplete_list_11').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE5565189692'))
            self.partialChecksum2 = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(self.partialChecksum == self.partialChecksum2)
        with self.subTest('kontrollerar återställning'):
            self.driver.find_element_by_id('producerReset').click()
            WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE5565189692'))
            resetChecksum = hashlib.md5(self.driver.find_elements_by_tag_name('tbody')[1].text.encode('utf-8')).hexdigest()
            self.assertTrue(fulltextChecksum == resetChecksum)
    def test_openAnchorLink(self):
        print('hittar och öppnar ankar-länken för mer information...')
        self.driver.get(self.prodURL)
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
        self.driver.find_element_by_link_text('Mera information om....').click()
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE23210000'))
        haveit = True       
        self.assertTrue(haveit)
    def test_select2ConsumersByLink(self):
        self.driver.get('https://qa.integrationer.tjansteplattform.se/?filter=c139,c834')
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'HSASERVICES'))
        colText = self.driver.find_element_by_id('consumerPlace').text
        colText = re.sub(r"[\n\t\s]*", "", colText)
        self.assertTrue(colText == 'IneraAB--Tjänsteplattform--NationellatjänsterHSASERVICES-106JRegionGotland--Hälso-ochsjukvårdsförvaltningenSE162120000803-B03')
    def test_select2DomainsByLink(self):
        self.driver.get('https://qa.integrationer.tjansteplattform.se/?filter=d17,d11')
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'HSASERVICES'))
        colText = self.driver.find_element_by_id('contractPlace').text
        colText = re.sub(r"[\n\t\s]*", "", colText)
        self.assertTrue(colText == 'clinicalprocess:activity:requestProcessRequestConfirmationv1ProcessRequestOutcomev1ProcessRequestv1crm:schedulingCancelBookingv1GetAllCareTypesv1GetAllHealthcareFacilitiesv1GetAllPerformersv1GetAllTimeTypesv1GetAvailableDatesv1GetAvailableTimeslotsv1GetBookingDetailsv1GetCancelledAndRebookedv1GetSubjectOfCareSchedulev1MakeBookingv1UpdateBookingv1')
    def test_select2platformsByLink(self):
        self.driver.get('https://qa.integrationer.tjansteplattform.se/?filter=F4L4,F3L3')
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'HSASERVICES'))
        colText = self.driver.find_element_by_id('chainPlace').text
        colText = re.sub(r"[\n\t\s]*", "", colText)
        self.assertTrue(colText == 'SLL-PRODSLL-QA')
    def test_select2LAsByLink(self):
        self.driver.get('https://qa.integrationer.tjansteplattform.se/?filter=l8221,l8')
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'HSASERVICES'))
        colText = self.driver.find_element_by_id('laPlace').text
        colText = re.sub(r"[\n\t\s]*", "", colText)
        self.assertTrue(colText == 'Gotland(länskod)09RegionÖrebrolänTSESE2321000164-1000')
    def test_select2ProducersByLink(self):
        self.driver.get('https://qa.integrationer.tjansteplattform.se/?filter=p297,p834')
        WebDriverWait(self.driver, 9).until(EC.text_to_be_present_in_element((By.ID, 'consumerForm'),'SE162321000164-0090'))
        colText = self.driver.find_element_by_id('producerPlace').text
        colText = re.sub(r"[\n\t\s]*", "", colText)
        print(colText)
        self.assertTrue(colText == 'RegionGotland--Hälso-ochsjukvårdsförvaltningenSE162120000803-B03RegionStockholm--EDISE2321000016-4HR3')

''' filtrering på två komponenter kan genomföras genom att ta två kända komponenter och leta efter kända strängar. '''

'''  
saker vi vill ha/göra
-
- jämför QA mot prod checksummor -

- alla webbläsare förutom netscape men gärna opera

- logga steg och rapport efter test

'''

if __name__ == '__main__':
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output=''))